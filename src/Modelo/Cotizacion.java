/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author _Enrique_
 */
public class Cotizacion {
    
    String folio, descripcion;
    float precio, porPago;
    int plazo; 
    
    public Cotizacion(){ 

    }
    public Cotizacion(String folio, String descripcion, float precio, float porPago, int plazo){
        this.folio=folio;
        this.descripcion=descripcion;
        this.precio=precio;
        this.porPago=porPago;
        this.plazo=plazo;        
    }
    public Cotizacion(Cotizacion cot){
        this.folio=cot.folio;
        this.descripcion=cot.descripcion;
        this.precio=cot.precio;
        this.porPago=cot.porPago;
        this.plazo=cot.plazo;
    }
    public void setFolio(String fol){
        this.folio=fol;
    }
    public void setDesc(String des){
        this.descripcion=des;
    }
    public void setPrecio(float pre){
        this.precio=pre;
    }
    public void setPorPagoI(float pagoI){
        this.porPago=pagoI;
    }
    public void setPlazo(int pla){
        this.plazo=pla;
    } 
    public String getFolio(){
        return folio;
    }
    public String getDesc(){
        return descripcion;
    }
    public float getPrecio(){
        return precio;
    }
    public float getPorPagoI(){
        return porPago;
    }
    public int getPlazo(){
        return plazo;
    }
    public float calcularPagoI(){
        float totalP=0;
        totalP=(this.precio/100)*this.porPago;
        return totalP;
    }
    public float calcularTotalF(){
        float totalF=0;
        totalF=this.precio-calcularPagoI();
        return totalF;
    }
    public float calcularPagoM(){
        float totalM=0;
        totalM=calcularTotalF()/this.plazo;
        return totalM;
    }
    
}
